What's this ?
=========
My practice following [Jetpack Compose basics].

Why I take it ?
=========
Compose is park of Jetpack and it's new although I think it is not helpful to my career but <br>
learning and experience new things are important to me, it helps me keep thinking and I enjoy it<br>
more and more.

Thought after this.
=========
Compose is an interesting idea, it tries to optimize performance of UI,<br>
I can feels it's telling me "Run the code if necessary", it also integrate <br>
theme and material design to make styling app easier.

[Jetpack Compose basics]:https://developer.android.com/codelabs/jetpack-compose-basics
